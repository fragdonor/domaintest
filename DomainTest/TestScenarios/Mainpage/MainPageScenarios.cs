﻿using System.Collections.Generic;
using System.Configuration;
using DomainTest.PageObjects;
using OpenQA.Selenium;

namespace DomainTest.TestScenarios.Mainpage
{
    public class MainPageScenarios
    {
        private List<string> tabsToTest = new List<string>()
        {
            "Property",
            "Business for Sale",
            "Franchise",
            "News"
        };

        public void DomainTopHeaderTestScenario(IWebDriver driver)
        {
            CommonFunctions.NavigateToUrl(driver, ConfigurationManager.AppSettings["DomainUrl"]);
            DomainMainPage domainMainPage = new DomainMainPage(driver);

            foreach (var tab in tabsToTest)
            {
                domainMainPage.TestTopHeaderLink(domainMainPage.getTopHeaderPropertyTab());
            }

            driver.Close();
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace DomainTest.Helpers
{
    public class DomainPageTitles
    {
        private readonly Dictionary<string, string> _domainPageTitles = new Dictionary<string, string>();

        public DomainPageTitles()
        {
            _domainPageTitles.Add(@"https://www.commercialrealestate.com.au/", "Commercial Real Estate and Property For Sale and Lease in Australia | CommercialRealEstate.com.au");
            _domainPageTitles.Add(@"https://www.commercialrealestate.com.au/business/", "Business for sale | Choose from over ");
            _domainPageTitles.Add(@"https://www.commercialrealestate.com.au/franchise/", "Franchise for sale | Choose from over");
            _domainPageTitles.Add(@"https://www.commercialrealestate.com.au/news/", "Commercial Property & Real Estate News");
        }

        public string GetTitleForUrl(string url)
        {
            var myKey = _domainPageTitles.FirstOrDefault(x => x.Key == url).Value;
            return myKey!=null ? myKey:"No Title was found";
        }
    }
}

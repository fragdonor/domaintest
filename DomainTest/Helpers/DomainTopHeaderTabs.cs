﻿using System.Collections.Generic;
using System.Linq;

namespace DomainTest.Helpers
{
    public class DomainTopHeaderTabs
    {

        private Dictionary<string, string> _domainTopHeaderTabs = new Dictionary<string, string> ();

        public DomainTopHeaderTabs()
        {
            _domainTopHeaderTabs.Add("Property", @"https://www.commercialrealestate.com.au/");
            _domainTopHeaderTabs.Add("Business for Sale", @"https://www.commercialrealestate.com.au/business/");
            _domainTopHeaderTabs.Add("Franchise", @"https://www.commercialrealestate.com.au/franchise/");
            _domainTopHeaderTabs.Add("News", @"https://www.commercialrealestate.com.au/news/");
        }

        public string GetUrlForTab(string tab)
        {
            var myKey = _domainTopHeaderTabs.FirstOrDefault(x => x.Key == tab).Value;
            return myKey != null ? myKey : "The tab was not found";
        }

    }
}

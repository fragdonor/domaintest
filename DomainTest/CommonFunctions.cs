﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Xunit;

namespace DomainTest
{
    public static class CommonFunctions  
    {
        #region Navigation
        public static void NavigateBack(IWebDriver driver)
        {
            driver.Navigate().Back();
            Console.WriteLine("Navigated back");
            WaitUntilJsPageLoaded(driver, 30);
        }
        public static void NavigateToUrl(IWebDriver driver,string url)
        {
            SetWaitingTime(driver, 30, 30);
            driver.Navigate().GoToUrl(url);
            Console.WriteLine("Navigated to "+url);
            WaitUntilJsPageLoaded(driver, 30000);

        }
        
        #endregion
        #region Waits
        public static void SetWaitingTime(IWebDriver driver, int implicitWait, int pageLoad)
        {
            SetPageLoadWaitSeconds(driver, pageLoad);
            SetImplicitlWaitSeconds(driver, implicitWait);
        }
        public static void SetImplicitlWaitSeconds(IWebDriver driver, int seconds)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }
        public static T Execute<T>(this IWebDriver driver, string script)
        {
            return (T)((IJavaScriptExecutor)driver).ExecuteScript(script);
        }
        public static void WaitUntilJsPageLoaded(IWebDriver driver, int millisec)
        {
            var time = 0;
            while (driver.Execute<bool>("return document.readyState != 'complete'")&&(millisec>time))
            {
                Thread.Sleep(100);
                time+= 100;
                Assert.True(millisec <= time, String.Format("Page was not loaded in {0} milliseconds", millisec));
            }
            Assert.True(millisec > time, String.Format("Page was loaded in {0} milliseconds", time));
        }
        public static void SetPageLoadWaitSeconds(IWebDriver driver, int seconds)
        {
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(seconds);
        }
        public static IWebElement WaitUntilAnyTextIsPresentInElement(IWebDriver driver, int seconds, By by, string text)
        {
            IWebElement element = driver.FindElement(by);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            wait.Until(ExpectedConditions.TextToBePresentInElement(element, text));
            return element;
        }
        
        public static void WaitUntilAnyTextIsPresentInElementValue(IWebDriver driver, int seconds, By by, string text)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            wait.Until(ExpectedConditions.TextToBePresentInElementValue(by, text));
        }
        public static IWebElement waitForElement(IWebDriver driver, int seconds, By locator)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(seconds)).Until(ExpectedConditions.ElementToBeClickable(locator));
        }
     

        public static void WaitForAjax(this IWebDriver driver, int timeoutSecs = 10, bool throwException = false)
        {
            for (var i = 0; i < (timeoutSecs * 10); i++)
            {
                var javaScriptExecutor = driver as IJavaScriptExecutor;
                var ajaxIsComplete = javaScriptExecutor != null && (bool)javaScriptExecutor.ExecuteScript("return jQuery.active == 0");
                if (ajaxIsComplete) return;
                Thread.Sleep(100);
            }
            if (throwException)
            {
                throw new Exception("WebDriver timed out waiting for AJAX call to complete");
            }
        }

        

        public static bool ElementExists(this IWebDriver driver, By condition, int timeSpan = 5)
        {
            bool isElementPresent = false;
            
            var driverWait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeSpan));
            driverWait.IgnoreExceptionTypes(typeof(WebDriverTimeoutException));
            isElementPresent = driverWait.Until(x => x.FindElements(condition).Any());

            return isElementPresent;
        }

        public static IWebElement FindElementAfterWait(this IWebDriver driver, By condition, int fromSeconds = 90)
        {
            bool isElementPresent = false;
            IWebElement singleElement = null;

            var driverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(fromSeconds));
            driverWait.IgnoreExceptionTypes(typeof(WebDriverTimeoutException));

            try
            {
                isElementPresent = driverWait.Until(ExpectedConditions.ElementToBeClickable(condition)) != null;

                if (isElementPresent)
                {
                    singleElement = driver.FindElement(condition);
                }
            }
            catch
            {
                // log any errors
            }

            return singleElement;
        }

        public static void WaitUntilElementToBeClickable(IWebDriver driver, int seconds, By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            wait.Until(ExpectedConditions.ElementToBeClickable(by));
        }
        public static void WaitUntilElementToBeVisible(IWebDriver driver, int seconds, By by)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            wait.Until(ExpectedConditions.ElementIsVisible(by));
        }


      
        #endregion
        #region Clicks
        public static void ClickRightButton(IWebDriver driver, IWebElement webElement)
        {
            Actions act = new Actions(driver); // where driver is WebDriver type
            act.MoveToElement(webElement).Perform();
            act.ContextClick().Perform();
            Console.WriteLine("Clicked right button on element");
        }
        #endregion
        #region Drag
        public static void DragNDrop(IWebDriver driver, By from, By to)
        {
            Actions act = new Actions(driver); // where driver is WebDriver type
            IWebElement source = driver.FindElement(from); 
            IWebElement target = driver.FindElement(to); 
            act.DragAndDrop(source, target).Perform();
            Console.WriteLine("Dragged element from - " + from +" to - "+to);
        }
        #endregion 
        #region Selects
        public static void SelectElementByValueFromDropDown(IWebDriver driver, By by,string value)
        {
            SelectElement dropdown = new SelectElement(driver.FindElement(by));
            dropdown.SelectByValue(value);
            Console.WriteLine("Selected in dropdown value - " + value);
        }
        public static void SelectElementByTextFromDropDown(IWebDriver driver, By by, string text)
        {
            SelectElement dropdown = new SelectElement(driver.FindElement(by));
            dropdown.SelectByText(text);
            Console.WriteLine("Selected in dropdown text - " + text);
        }
        public static void SelectElementByIndexFromDropDown(IWebDriver driver, By by, int id)
        {
            SelectElement dropdown = new SelectElement(driver.FindElement(by));
            dropdown.SelectByIndex(id);
            Console.WriteLine("Selected in dropdown index - " + id);
        }
        #endregion
        #region Gets
        public static string GetCurrentUrl(IWebDriver driver)
        {
            var url = driver.Url;
            Console.WriteLine("Got page url - " + url);
            return driver.Url;
        }

        public static string GetTitle(IWebDriver driver)
        {
            var title = driver.Title;
            Console.WriteLine("Got page title - " + title);
            return title;
        }
        public static string GetTextboxText(IWebDriver driver, By by)
        {
            var val = driver.FindElement(by).GetAttribute("value");
            Console.WriteLine("Got element value - " + val);
            return val;
        }

        #endregion
    }
}

﻿using System.Configuration;
using DomainTest.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Xunit;

namespace DomainTest.PageObjects
{
    public class DomainMainPage
    {
        private IWebDriver _driver;
        private readonly DomainTopHeaderTabs _domainTopHeaderTabs = new DomainTopHeaderTabs();
        private readonly DomainPageTitles _domainPageTitles = new DomainPageTitles();

        [FindsBy(How = How.LinkText, Using = "Property")]
        private IWebElement topHeaderPropertyTab { get; set; }

        [FindsBy(How = How.LinkText, Using = "Business for Sale")]
        private IWebElement topHeaderBusinessForSaleTab { get; set; }

        [FindsBy(How = How.LinkText, Using = "Franchise")]
        private IWebElement topHeaderFranchiseTab { get; set; }
        
        [FindsBy(How = How.LinkText, Using = "News")]
        private IWebElement topHeaderNewsTab { get; set; }


        public IWebElement getTopHeaderPropertyTab()
        {
            return topHeaderPropertyTab;
        }
        public IWebElement getTopHeaderBusinessForSaleTab()
        {
            return topHeaderBusinessForSaleTab;
        }
        public IWebElement getTopHeaderFranchiseTab()
        {
            return topHeaderFranchiseTab;
        }
        public IWebElement getTopHeaderNewsTab()
        {
            return topHeaderNewsTab;
        }
        public DomainMainPage(IWebDriver driver)
        {
            _driver=driver;
            PageFactory.InitElements(driver, this);
            CommonFunctions.SetWaitingTime(_driver,30,30);
        }
        public void TestTopHeaderLink(IWebElement element)
        {
            var savedUrl = _domainTopHeaderTabs.GetUrlForTab(element.Text);
            Assert.Equal(element.GetAttribute("href"), savedUrl);
            var savedTitle = _domainPageTitles.GetTitleForUrl(element.GetAttribute("href"));
            element.Click();
            CommonFunctions.WaitUntilJsPageLoaded(_driver, 30000);
            Assert.Equal(CommonFunctions.GetCurrentUrl(_driver), savedUrl);
            Assert.Contains(savedTitle, CommonFunctions.GetTitle(_driver));
            if (CommonFunctions.GetCurrentUrl(_driver)!= ConfigurationManager.AppSettings["DomainUrl"]) CommonFunctions.NavigateBack(_driver);

        }
    }
}

﻿using DomainTest.TestScenarios.Mainpage;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using Xunit;

namespace DomainTest.Tests.IE
{
    
    public class RunInIe 
    {
        private readonly MainPageScenarios _scenario = new MainPageScenarios();


        [Fact]
        public void DomainTopHeaderTest()
        {
            IWebDriver driver = new InternetExplorerDriver();
            CommonFunctions.SetWaitingTime(driver, 30, 30);
            driver.Manage().Window.Maximize();
            _scenario.DomainTopHeaderTestScenario(driver);
        }

    }
}

﻿using DomainTest.TestScenarios.Mainpage;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using Xunit;

namespace DomainTest.Tests.Chrome
{

    public class RunInChrome
    {
        private readonly MainPageScenarios _scenario = new MainPageScenarios();
        
        
        [Fact]
        public void DomainTopHeaderTest()
        {
            IWebDriver driver = new ChromeDriver();
            CommonFunctions.SetWaitingTime(driver,30,30);
            driver.Manage().Window.Maximize();
            _scenario.DomainTopHeaderTestScenario(driver);
        }
    }
}

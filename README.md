# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a test project for checking header tabs on https://www.commercialrealestate.com.au/

### How do I get set up? ###

* Clone repo with git, SourceTree(HTTPS - SSH) or gitExtensions
* Open in VS2015 - DomainTest.sln
* Build solution
** In case references are missing - install from Nuget manager following
*** xunit
*** xunit.runner.visualstudio
*** Selenium.WebDriver
*** Selenium.WebDriver.ChromeDriver
*** Selenium.WebDriver.IEDriver
*** Selenium.Support


* Click in menu Test-> Window-> Test Explorer.
* In the TestExplorer - click Run All (First run for IE may fail due to initial Firewall/IE settings, run it again)

### Who do I talk to? ###

* Andriy Rud rud.andriy@gmail.com
